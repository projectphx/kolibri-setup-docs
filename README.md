# Kolibri-Setup-Docs

These are docs explaining how our Kolibri servers can be setup.
The purpose of this is for more advanced users and talking about content that I couldn't find in the Kolibri docs nor Kolibri dev-docs.  
*Writer's notes*: The Raspberry Pi documentation has much more information than the other installs.

# Installation
This document assumes you have already installed Debian or Ubuntu Linux.  
If not, do so before beginning.  

There is a script in this folder that should be run to do the install.  
The commands in the script originated from the [Kolibri Install Documents](https://kolibri.readthedocs.io/en/latest/install/index.html).
The script is those commands in script form, starting with an update to the OS.
To run the script:
```
# These are essentially the steps for installation in script form
./setupKolibri.sh
```

Kolibri use NginX to distribute web content.  
It provides a file that /etc/nginx/nginx.conf reads called /etc/nginx/conf.d/kolibri.conf,  
which links to a file in /etc/kolibri/nginx.d/[number]-user.conf,  
Which imports from the nginx.conf file that runs the server at ~/.kolibri/nginx.conf

## During install
If you are on Ubuntu, adding a repository makes you confirm your addition. Confirm it.

You will come across various settings that Kolibri asks for user input on.  
For now, defaults on each of these will work.
The computer will `reboot` at the end of the script, and Kolibri should be available on port 8080.

# After Installation
## Move to port 80
There is one file that need to be edited.  
~/.kolibri/options.ini  
```
HTTP_PORT = 8080
||
\/
HTTP_PORT = 80
```

This alone will direct the user to the default Nginx page.  
So, we also need to remove /etc/nginx/sites-enabled/default
```
sudo rm /etc/nginx/sites-enabled/default
```

Then `reboot` the system.  
It should come back up with Kolibri on port 80.

# Implementing SSL and HTTPS
Before doing anything, make sure the Domain we are using (learning.azblockchain.org) directs to our server's IP.  
Instructions for this can be found in many places, and it varies too much for me to be able to write it here.  
Once the A record is on this server's IP, proceed from here.  

## Obtaining SSL Cert
For this, I used Certbot to obtain LetsEncrypt SSL certificates.  
The strange design of Kolibri's Nginx setup also means that we can not use Certbot to auto-configure nginx for us.  
(If you figure out how, let me know. 
Certbot has a lot of options, thus I may just not know it)  

To obtain a cert using LetsEncrypt, run this command and follow the prompts with fitting questions.
```
sudo Certbot --nginx 
# For tests, please use either --dry-run and/or --test-cert as well.
# Certbot limits how many actual certs one can obtain in a given time span.
# Note: Our domain is learning.azblockchain.org
```

## Distributing over HTTPS
Since we now have the certificates, we can now use HTTPS.
Add these lines to /etc/kolibri/nginx.d/###-user.conf:
```
listen 443 ssl http2;
ssl_certificate     /etc/letsencrypt/live/learning.azblockchain.org/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/learning.azblockchain.org/privkey.pem;
```

From here, it may also be desirable to change the HTTP_PORT option mentioned earlier from 80 to 443.
~/.kolibri/options.ini  
```
HTTP_PORT = 80
||
\/
HTTP_PORT = 443
```

Once again, `reboot` the device, and content should now be distributed on HTTPS.

## Hardening Advice
[Mozilla has a useful page for this](https://ssl-config.mozilla.org/)  

As admin you need to determine which implementation is best for your use case.  
ACKNOWLEDGE: HSTS can prevent one from returning to HTTP. 
If you have that option set, you MUST clear your browser's cache before trying to go to HTTP, again.

There is also the automatic redirect from HTTP to HTTPS as an option. 
In our case, make the file /etc/nginx/sites-enabled/kolibri and add the following contents:  
/etc/nginx/sites-enabled/kolibri_redirect
```
server {
    listen 80 default_server;
    listen [::]:80 default_server;
    server_name _; # For any servername that comes here
    return 301 https://$host$request_uri; #redirect to HTTPS version
}
```
Port 80 is also open on this server, as a result, and it will redirect traffic to HTTPS on 443, when users go there.

# Troubleshooting
The current greatest trouble is from Kolibri's automatic rewriting of ~/.kolibri/nginx.conf.  
So, if you have trouble with nginx saying "duplicate option",  
or the page simply not coming up,  
or the page coming up on the same port as previously stopped...  
Mess around with ~/.kolibri/nginx.conf's HTTP_PORT option.  

Check Nginx's syntax with
```
sudo nginx -t
#or for more info
sudo nginx -T
```
