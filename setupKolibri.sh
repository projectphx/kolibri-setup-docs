#!/bin/sh
sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y
sudo apt install -y dirmngr
sudo su -c 'echo "deb http://ppa.launchpad.net/learningequality/kolibri/ubuntu bionic main" > /etc/apt/sources.list.d/learningequality-ubuntu-kolibri.list'
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys DC5BAA93F9E4AE4F0411F97C74F88ADB3194DD81
sudo apt update
sudo apt install -y kolibri-server 
sudo reboot
