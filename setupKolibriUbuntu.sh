#!/bin/sh
sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y
sudo apt install -y dirmngr software-properties-common
sudo add-apt-repository ppa:learningequality/kolibri
sudo apt install -y kolibri-server 
sudo reboot
